---
title: My first Jekyll page
---

# Building Websites

## Description

{{site.description}}

Welcome to {{page.title}}.

Hello World!

[Mail me (but also not)](mailto:{{site.email}})

Here is the list of files that I had to create:
- a .gitlab-ci.yaml file
- a Gemfile
- an index.md
- a _config.yml

More details in the [About page](about)